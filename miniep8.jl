## MAC0110 - MiniEP 8
## Aluno: Rodrigo Volpe Battistin
## NUSP: 11795464

function troca(v, i, j) #Função dada em aula
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end
function insercao(v) #Função dada em aula
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1]) #Ordena por naipe e valor
				troca(v, j, j - 1)
			else
				break
			end
		j = j - 1
		end
	end
	return v
end

#Parte 1
using Test
function testaByValue()
	@test compareByValue("2♠", "A♠") == true
	@test compareByValue("K♡", "10♡") == false
	@test compareByValue("10♠", "10♡") == false
	@test compareByValue("2♢", "3♣") == true
	@test compareByValue("J♣", "K♣") == true
	@test compareByValue("A♡", "10♢") == false
	@test compareByValue("A♡", "K♣") == false
	println( insercao(["10♡", "10♢", "K♣", "A♣", "J♣", "A♣"]) )	
	println( insercao(["4♡", "7♢", "7♣", "A♣", "J♡", "K♣"]) )
	println( insercao(["1♠", "4♠", "2♠", "3♠", "A♠", "9♠"]) )
	println( insercao(["10♡", "10♢", "10♣", "10♠"]) )
	println("Acabaram os testes")
end

function letterToValue(letter) #Função auxiliar
	if letter == "J"
		return 11
	end
	if letter == "Q"
		return 12
	end
	if letter == "K"
		return 13
	end
	if letter == "A"
		return 14
	end
end

function compareByValue(x, y)
	string1 = x[1:(end - 1)]
	string2 = y[1:(end - 1)]
	valor1 = tryparse(Int, string1)
	valor2 = tryparse(Int, string2)
	if valor1 == nothing
		valor1 = letterToValue(string1)
	end
	if valor2 == nothing
		valor2 = letterToValue(string2)
	end
	if valor1 < valor2
		return true
	else
		return false
	end
end

#testaByValue()

#Parte 2
function testaValueNSuit()
	@test compareByValueAndSuit("2♠", "A♠") == true
	@test compareByValueAndSuit("K♡", "10♡") == false
	@test compareByValueAndSuit("10♠", "10♡") == true
	@test compareByValueAndSuit("Q♢", "3♣") == true
	@test compareByValueAndSuit("J♣", "K♡") == false
	@test compareByValueAndSuit("A♡", "10♢") == false
	@test compareByValueAndSuit("A♡", "K♠") == false
	println( insercao(["10♡", "10♢", "K♣", "A♣", "J♣", "A♣"]) )	
	println( insercao(["4♡", "7♢", "7♣", "A♣", "J♡", "K♣"]) )
	println( insercao(["1♠", "4♠", "2♠", "3♠", "A♠", "9♠"]) )
	println( insercao(["10♡", "10♢", "10♣", "10♠"]) )
	println("Acabaram os testes")
end

function compareByValueAndSuit(x, y)
	ouros = '♢'
	espadas = '♠'
	copas = '♡'
	paus = '♣'
	naipe1 = x[end]
	naipe2 = y[end]

	if naipe1 == naipe2
		return compareByValue(x, y)
	else
		if naipe1 == ouros || naipe2 == paus
			return true
		end
		if naipe1 == paus || naipe2 == ouros
			return false
		end
		if naipe1 == espadas
			return true
		else
			return false
		end
	end
end

#testaValueNSuit()
